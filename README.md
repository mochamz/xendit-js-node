# xendit-js-node

## Getting started

```
npm install --save xendit-js-node
```

### Example

Example on how to use this plugin, can be seen on this 
[react-native example app](https://github.com/xendit/xendit-react-example)
